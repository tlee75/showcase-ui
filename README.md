# showcase-ui

This project contains a UI written in Vue.js for my professional profile and provides a glimpse at some of the technologies 
I have experience with. This project depends on infrastructure deployed by other projects listed below.

Code pushed to the master branch utilizes GitLab CI to build and deploy the distribution to Amazon S3 and 
becomes viewable online.

Staging: [https://www.staging-tylerlee.dev](https://www.staging-tylerlee.dev)  
Prod: [https://www.tylerlee.dev](https://www.tylerlee.dev)

# Technology stack

Vue.js
GitLab CI
S3

# Related Projects

[Front end Infrastructure](https://gitlab.com/tlee75/showcase-frontend)  
